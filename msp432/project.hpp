#pragma once

struct Project
{
	virtual int run()=0;
};
