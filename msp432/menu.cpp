/* #include "menu.hpp" */

/* Display::Display(gpio_msp432_pin * _lcd_cs, spi_msp432 * _spi, gpio_msp432_pin * _lcd_rst, gpio_msp432_pin * _lcd_dc, st7735s_drv * _lcd, uGUI * _gui): lcd_cs(_lcd_cs), spi(_spi), lcd_rst(_lcd_rst), lcd_dc(_lcd_dc), lcd(_lcd), gui(_gui) */
/* {} */

/* Display::~Display() */
/* { */
/* 	delete lcd_cs; */
/* 	delete spi; */
/* 	delete lcd_rst; */
/* 	delete lcd_dc; */
/* 	delete lcd; */
/* 	delete gui; */
/* } */

/* Display* create_display_default() */
/* { */
/* 	// Setup SPI interface */
/* 	gpio_msp432_pin * lcd_cs = new gpio_msp432_pin(PORT_PIN(5, 0)); */
/* 	spi_msp432 * spi = new spi_msp432(EUSCI_B0_SPI, *lcd_cs); */
/* 	spi->setSpeed(24000000); */
/* 	// Setup LCD driver */
/* 	gpio_msp432_pin * lcd_rst = new gpio_msp432_pin(PORT_PIN(5, 7)); */
/* 	gpio_msp432_pin * lcd_dc  = new gpio_msp432_pin(PORT_PIN(3, 7)); */

/* 	st7735s_drv * lcd = new st7735s_drv(*spi, *lcd_rst, *lcd_dc, st7735s_drv::Crystalfontz_128x128); */
/* 	// Setup uGUI */
/* 	uGUI * gui = new uGUI(*lcd); */
/* 	Display * out = new Display(lcd_cs, spi, lcd_rst, lcd_dc, lcd, gui); */
/* 	return out; */
/* } */


/* Widget::Widget(Display* display, Area* area): display(display), area(area) */
/* {} */

/* Widget::~Widget() */
/* { */
/* 	delete area; */
/* } */

/* WidgetStatusline::WidgetStatusline(Display* display, Rectangle* area): Widget(display, area) */
/* {} */

/* void WidgetStatusline::print_statusline_sites(String mode, int position, int len) */
/* { */
/* 	int xs = ((Rectangle*)area)->xs; */
/* 	int xe = ((Rectangle*)area)->xe; */
/* 	int ys = ((Rectangle*)area)->ys; */
/* 	int ye = ((Rectangle*)area)->ye; */
/* 	int status_line_h = ((Rectangle*)area)->height(); */
/* 	display->gui->SetForecolor(C_GREEN); */
/* 	display->gui->FontSelect(&FONT_6X8); */
/* 	int font_x = 8; */
/* 	int font_y = 6; */
/* 	int pl = 2; // padding left */
/* 	int pr = 2; // padding right */
/* 	int pt = ((status_line_h-1/1*border top 1px*1/) - font_y) / 2; // padding top */
/* 	display->gui->DrawLine(xs, ys, xe, ys, C_GREEN); */
/* 	display->gui->PutString(pl, ys+pt, mode); */
/* 	String site = to_String(position) + "/" + to_String(len); */
/* 	// TODO: wieso 7? */
/* 	int site_len = site.size()*(font_x-1); */
/* 	display->gui->PutString((xe-1)-site_len-pr, (ys+1)+pt, site); */
/* } */

/* WidgetStatusline* create_widget_statusline_default(Display* display, unsigned int height) */
/* { */
/* 	unsigned int ye = display->lcd->getSizeY()-1; */
/* 	Rectangle* rec = create_rectangle(0, ye-height, display->lcd->getSizeX()-1, ye); */
/* 	WidgetStatusline* out = new WidgetStatusline(display, rec); */
/* 	return out; */
/* } */


/* WidgetMessageList::WidgetMessageList(Display* display, Rectangle* area, Vector<Message*> * msgs) */
/* 	: Widget(display, area), msgs(msgs) */
/* {} */

/* WidgetMessageList::~WidgetMessageList() */
/* { */
/* 	for(Message* msg: *msgs) */
/* 		delete msg; */
/* 	delete msgs; */
/* } */

/* void WidgetMessageList::setup() */
/* { */
/* 	display->gui->SetForecolor(C_YELLOW); */
/* 	display->gui->FontSelect(&FONT_5X8); */
/* } */

/* void WidgetMessageList::print_msgliste(int selected_msg) */
/* { */
/* 	int height = 8; */
/* 	int pl = 4; // padding left */
/* 	int pr = 6; // padding right */
/* 	int pt = 4; // padding top */
/* 	int page = selected_msg / items_per_page; */
/* 	int offset = page*items_per_page; */
/* 	unsigned int box_x = ((Rectangle*) area)->height(); */
/* 	unsigned int box_y = ((Rectangle*) area)->width(); */
/* 	display->gui->FontSelect(&FONT_5X8); */
/* 	/1* CharBox box(20,1); *1/ */
/* 	/1* msg->message->toBoxStrings(box, true, 3, false) *1/ */
/* 	for(unsigned int i=0; i<msgs->size()-offset; i++) */
/* 	{ */
/* 		Message * msg = (*msgs)[i+offset]; */
/* 		int text_x = pt+(height+3)*i; */
/* 		int line_x = pt+(height+3)*(i+1)-2; */
/* 		if (line_x<box_y) { */
/* 			if (i+offset==selected_msg) */
/* 			{ */
/* 				display->gui->SetForecolor(C_LIGHT_GREEN); */
/* 				String text = ">"; */
/* 				text += msg->message; */
/* 				display->gui->PutString(pl, text_x, text); */
/* 			} else { */
/* 				display->gui->SetForecolor(C_GREEN); */
/* 				display->gui->PutString(pl, text_x, msg->message); */
/* 			} */
/* 			if (i+offset==selected_msg || i+offset==selected_msg-1) */
/* 			{ */
/* 				display->gui->DrawLine(pl, line_x, box_x-pr, line_x, 0x555555); */
/* 			} else */
/* 			{ */
/* 				display->gui->DrawLine(pl, line_x, box_x-pr, line_x, 0x111111); */
/* 			} */
/* 		} */
/* 	} */
/* } */

/* WidgetMessageList * create_widget_messagelist_default(Display* display, unsigned int statusline_h) */
/* { */
/* 	unsigned int height = display->lcd->getSizeX(); */
/* 	unsigned int width = display->lcd->getSizeY()-statusline_h; */
/* 	Rectangle * rect = create_rectangle(height, width); */
/* 	WidgetMessageList* out = new WidgetMessageList(display, rect); */
/* 	out->setup(); */
/* 	return out; */
/* } */


/* Menu::Menu(Display* display): display(display) */
/* {} */

/* MenuMessageList::MenuMessageList(Display* display, WidgetStatusline* statusline, WidgetMessageList* messagelist) */
/* 	: Menu(display), statusline(statusline), messagelist(messagelist) */
/* {} */

/* void MenuMessageList::print() */
/* { */
/* 	unsigned int items_per_page = 10; */
/* 	unsigned int size = messagelist->msgs->size()-1; */
/* 	unsigned int selected_msg = size; */
/* 	unsigned int current_site = selected_msg/items_per_page + 1; */
/* 	unsigned int amount_sites = (size)/items_per_page + 1; */
/* 	display->lcd->clearScreen(0x0); */
/* 	messagelist->print_msgliste(selected_msg); */
/* 	statusline->print_statusline_sites("Received", current_site, amount_sites); */
/* } */

/* void MenuMessageList::add_message(Message * message) */
/* { */
/* 	messagelist->msgs->push_back(message); */
/* } */

/* MenuMessageList* create_menu_messagelist_default(Display* display) */
/* { */
/* 	WidgetStatusline * statusline = create_widget_statusline_default(display); */
/* 	WidgetMessageList * messagelist = create_widget_messagelist_default(display); */
/* 	MenuMessageList* out = new MenuMessageList(display, statusline, messagelist); */
/* 	return out; */
/* } */
