#!/bin/bash
if [[ -e compile_commands.json ]]; then
	cp compile_commands.json .compile_commands.json~
fi
echo "[" > compile_commands.json

first=1

make clean
make | while read in; do
	if [[ `echo $in | grep "^C++ .*.cpp$"` ]]; then
		filename=`echo $in | sed "s/^C++ //g"`
	fi
	if [[ `echo $in | grep "^C .*.c$"` ]]; then
		filename=`echo $in | sed "s/^C //g"`
	fi
	if [[ `echo $in | grep "\@"` ]]; then
		# echo $in;
		cmd=`echo ${in:2} | sed "s/-march=[^ ]* //g"`
		if [[ $first -eq 1 ]]; then
			echo "{" >> compile_commands.json
			first=0
		else
			echo ",{" >> compile_commands.json
		fi
		echo "\"directory\": \"$(pwd)\",\"command\": \"${cmd} -DLANGSERV\",\"file\": \"$(pwd)/${filename}\""} >> compile_commands.json
		# add a line with the hpp file and the command to compile the cpp/c file
		filename_hpp=`echo $filename|sed "s/.cpp$/.hpp/"`
		if [[ $(ls "$(pwd)/${filename_hpp}" 2>/dev/null) ]];
		then
			if [[ $filename_hpp != $filename ]];
			then
				echo ",{\"directory\": \"$(pwd)\",\"command\": \"${cmd} -DLANGSERV\",\"file\": \"$(pwd)/${filename_hpp}\""} >> compile_commands.json
			fi
		fi
		# add a line with the h file and the command to compile the cpp/c file
		filename_h=`echo $filename|sed "s/.c$/.h/"`
		if [[ $(ls "$(pwd)/${filename_h}" 2>/dev/null) ]];
		then
			if [[ $filename_h != $filename ]];
			then
				echo ",{\"directory\": \"$(pwd)\",\"command\": \"${cmd}\",\"file\": \"$(pwd)/${filename_h}\""} >> compile_commands.json
			fi
		fi
	fi
done

echo "]" >> compile_commands.json
