#include "hardware.hpp"
#include "project.hpp"
#include "project_main.hpp"
#include "project_testing.hpp"

Project * get_project()
{
	bool testing = true;
	Project * project;
	Msp * msp = Msp::create_default();
	if (testing) {
		project = Testing::create_default(msp);
	} else {
		project = Main::create_default();
	}
	return project;
}

int main()
{
	auto project = get_project();
	auto out = project->run();
	return out;
}
