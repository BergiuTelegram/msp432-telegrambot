#include "geometrics.hpp"

// ---
// BOX
// ---
Box::Box(unsigned int width, unsigned int height): width(width), height(height)
{}

PixelBox::PixelBox(unsigned int width, unsigned int height): Box(width, height)
{}

CharBox::CharBox(unsigned int width, unsigned int height): Box(width, height)
{}

// ---------
// RECTANGLE
// ---------
// CON/DESTRUCTORS
Rectangle::Rectangle(unsigned int xs, unsigned int ys, unsigned int xe, unsigned int ye)
: Area(), xs(xs), ys(ys), xe(xe), ye(ye)
{}
Rectangle::~Rectangle()
{}

// GEOMETRIC DETAILS
unsigned int Rectangle::width()
{ return xe-xs; }
unsigned int Rectangle::height()
{ return ye-ys; }

// GEOMETRIC OPERATIONS
/* void move(int dir, int pixels); */

// OTHER
CharBox* Rectangle::create_biggest_box(unsigned int char_width, unsigned int char_height)
{
	//TODO: check if implemented right
	int b_width = width() / char_width;
	int b_height = height() / char_height;
	CharBox * out = new CharBox(b_width, b_height);
	return out;
}

// CREATE FUNCTIONS
/**
 * Creates a Rectangle in the area of xs to xe and ys to ye.
 * If the start points are bigger than the endpoints, they will be swapped
 * to prevent the creation of negative sizes inside the box(example: xe<xs).
 **/
Rectangle* create_rectangle(unsigned int xs, unsigned int ys, unsigned int xe, unsigned int ye)
{
	if(xs>xe)
		swap(xs, xe);
	if(ys>ye)
		swap(ys, ye);
	return new Rectangle(xs, ys, xe, ye);
}

/**
 * Creates a Rectangle with the width=SIZE_X and the height=SIZE_Y.
 * Start points xs and ys are set to 0.
 **/
Rectangle* create_rectangle(unsigned int size_x, unsigned int size_y)
{
	return new Rectangle(0, 0, size_x, size_y);
}

/**
 * Creates a Rectangle with the dimensions of the box.
 * Start points xs and ys are set to 0.
 **/
Rectangle* create(PixelBox & box)
{
	return new Rectangle(0, 0, box.width, box.height);
}
