#pragma once

#include <cstring>
#include <cstdio>
#include <queue>
#include <cstdlib>
#include <string.h>

#include <uart_msp432.h>
#include <std_io.h>
#include <gpio_msp432.h>
#include <spi_msp432.h>
#include <String.h>

#include <st7735s_drv.h>
#include <uGUI.h>
#include <uGUI_colors.h>
#include <font_4x6.h>
#include <font_5x8.h>
#include <font_5x12.h>
#include <font_6x8.h>
#include <font_6x10.h>
#include <font_7x12.h>
#include <font_8x12.h>

#include "util.hpp"
#include "vector.hpp"
#include "geometrics.hpp"
#include "communication.hpp"
#include "bot.hpp"
#include "menu.hpp"
#include "hardware.hpp"
#include "project.hpp"

struct Testing: Project
{
	Msp * msp;
	String * nul_cmd;
	String * snd_cmd_prefix;
	String * snd_cmd_suffix;
	String * mak_cmd_prefix;
	String * mak_cmd_suffix;

	Testing(Msp * _msp=nullptr, String * _nul_cmd=nullptr,
			String * _snd_cmd_prefix=nullptr, String * _snd_cmd_suffix=nullptr,
			String * _mak_cmd_prefix=nullptr, String * _mak_cmd_suffix=nullptr)
		: msp(_msp), nul_cmd(_nul_cmd), snd_cmd_prefix(_snd_cmd_prefix),
		snd_cmd_suffix(_snd_cmd_suffix), mak_cmd_prefix(_mak_cmd_prefix),
		mak_cmd_suffix(_mak_cmd_suffix)
	{}

	~Testing()
	{
		delete mak_cmd_suffix;
		delete mak_cmd_prefix;
		delete snd_cmd_suffix;
		delete snd_cmd_prefix;
		delete nul_cmd;
		delete msp;
	}

	static Testing * create_default(Msp * msp)
	{
		auto nul_cmd = new String("NUL;");
		auto snd_cmd_prefix = new String("SND;");
		auto snd_cmd_suffix = new String(";23767443;testsndcmd");
		auto mak_cmd_prefix = new String("SND;");
		auto mak_cmd_suffix = new String(";");
		auto test = new Testing(msp, nul_cmd, snd_cmd_prefix, snd_cmd_suffix,
				mak_cmd_prefix, mak_cmd_suffix);
		return test;
	}

	String * transfer(String * send_str)
	{
		// create buffers
		uint8_t sendbuf[100] = {0};
		uint8_t recvbuf[100] = {0};
		// write buffers
		/* strcpy((char *)sendbuf, sendbuf.c_str()); */
		int i;
		for (i = 0; i < 100; i++) {
			if (i<send_str->size()) {
				sendbuf[i] = (*send_str)[i];
			} else {
				sendbuf[i] = 0;
			}
			recvbuf[i] = 0;
		}
		// transfer
		msp->esp_con->esp_spi_con->esp_spi->transfer(sendbuf, recvbuf, 100);
		// create return
		String * recv = new String((const char *)recvbuf);
		return recv;
	}

	int parse_recv(String * recv)
	{
	}

	void sendnul()
	{
		String * send_str = new String(*nul_cmd);
		auto recv_str = transfer(send_str);
	}

	void sendsnd(int task_id)
	{
		String * send_str = new String(*nul_cmd);
		auto recv_str = transfer(send_str);
	}

	void sendmak()
	{
	}

	int run()
	{
		while(true)
		{
		}
		/* int id = '0'; */
		/* String sendmsg; */
		/* int a=0; */

		/* Message * m = new Message("0","0","MSP Info", "Initialisiere..."); */
		/* menu->add_message(m); */
		/* menu->print(); */

		/* while (true) { */
			/* uint8_t sendbuf[100] = {0}; */
			/* uint8_t recvbuf[100] = {0}; */
		/* 	if(a==0) */
		/* 	{ */
		/* 		sendmsg = */
		/* 		a++; */
		/* 	} else if(a==1) */
		/* 	{ */
		/* 		s_pref = "NUL"; */
		/* 		a++; */
		/* 	} else if(a==2) */
		/* 	{ */
		/* 		s_pref = "MAK"; */
		/* 		a=0; */
		/* 	} */
		/* 	String s(s_pref); */
		/* 	s += ";taskid;12345678;Message von MSP\0"; */
		/* 	for (int i = 0; i < 100; i++) { */
		/* 		if (i<s.size()) { */
		/* 			sendbuf[i] = s[i]; */
		/* 		} else { */
		/* 			sendbuf[i] = 0; */
		/* 			recvbuf[i] = 0; */
		/* 		} */
		/* 	} */
		/* 	/1* strcpy((char *)sendbuf, s.c_str()); *1/ */
		/* 	esp_spi.transfer(sendbuf, recvbuf, 100); */
		/* 	/1* printf("Message von ESP: %s", recvbuf); *1/ */
		/* 	String recv((const char *)recvbuf); */
		/* 	int pos = recv.find(';'); */
		/* 	String type = recv.substr(0, pos); */
		/* 	String tmp = recv.substr(pos+1); */
		/* 	if(type == String("NEW")){ */
		/* 		pos = tmp.find(';'); */
		/* 		String task_id = tmp.substr(0, pos); */
		/* 		tmp = tmp.substr(pos+1); */
		/* 		pos = tmp.find(';'); */
		/* 		String chat_id = tmp.substr(0, pos); */
		/* 		tmp = tmp.substr(pos+1); */
		/* 		pos = tmp.find(';'); */
		/* 		String from = tmp.substr(0, pos); */
		/* 		tmp = tmp.substr(pos+1); */
		/* 		pos = tmp.find(';'); */
		/* 		String s_message = tmp.substr(0, pos); */
		/* 		tmp = tmp.substr(pos+1); */
		/* 		Message * message = new Message(task_id, chat_id, from, s_message); */
		/* 		menu->add_message(message); */
		/* 	} else */
		/* 	{ */
		/* 		Message * message = new Message("0", "0", "MSP Error", "Not Supported type: "+type); */
		/* 		menu->add_message(message); */
		/* 	} */
		/* 	menu->print(); */
		/* 	if (id >= '9') { */
		/* 		id = '0'; */
		/* 	} else { */
		/* 		id++; */
		/* 	} */
		/* } */
		return 0;
	}
};

