#pragma once

#include <queue>

// swaps a and b
template <typename T>
void swap(T& a, T& b)
{
	T tmp(std::move(a));
	a = std::move(b);
	b = std::move(tmp);
}
