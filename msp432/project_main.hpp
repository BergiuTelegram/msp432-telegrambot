#pragma once

#include "project.hpp"

struct Main: Project
{
	Main()
	{}

	~Main()
	{}

	static Main * create_default()
	{
		auto out = new Main();
		return out;
	}

	int run()
	{
		return 1;
	}
};

