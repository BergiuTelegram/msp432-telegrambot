#pragma once

#include "util.hpp"

/**
 * A Box is stupid box that contains a width and a height.
 **/
struct Box
{
	unsigned int width;
	unsigned int height;
	Box(unsigned int width=0, unsigned int height=0);
};

/**
 * A Box that contains a width and a height.
 *
 * The width and height are the amount of pixels that fit in the box.
 **/
struct PixelBox: Box
{
	PixelBox(unsigned int width=0, unsigned int height=0);
};

/**
 * A Box that contains a width and a height.
 *
 * The width and height are the amount of chars that fit in the box.
 **/
struct CharBox: Box
{
	CharBox(unsigned int width=0, unsigned int height=0);
};

/**
 * An area of the display
 **/
struct Area
{};

/**
 * A rectangle is an area on the display described by pixels.
 * It is used to position widgets.
 * You can also use it to create the biggest CharBox
 * that fits into an area.
 **/
struct Rectangle: Area
{
	unsigned int xs; // x start, x:width
	unsigned int ys; // y start, y:height
	unsigned int xe; // x end
	unsigned int ye; // y end

	// CON/DESTRUCTORS
	protected:
	Rectangle(unsigned int xs, unsigned int ys, unsigned int xe, unsigned int ye);
	public:
	~Rectangle();

	// GEOMETRIC DETAILS
	unsigned int width();
	unsigned int height();
	// GEOMETRIC OPERATIONS
	/* void move(int dir, int pixels); */
	// OTHER
	CharBox* create_biggest_box(unsigned int char_width, unsigned int char_height);

	// CREATE FUNCTIONS
	friend Rectangle* create_rectangle(unsigned int xs, unsigned int ys, unsigned int xe, unsigned int ye);
	friend Rectangle* create_rectangle(unsigned int size_x, unsigned int size_y);
	friend Rectangle* create(PixelBox & box);
};

/**
 * Creates a Rectangle in the area of xs to xe and ys to ye.
 * If the start points are bigger than the endpoints, they will be swapped
 * to prevent the creation of negative sizes inside the box(example: xe<xs).
 **/
Rectangle* create_rectangle(unsigned int xs, unsigned int ys, unsigned int xe, unsigned int ye);

/**
 * Creates a Rectangle with the width=SIZE_X and the height=SIZE_Y.
 * Start points xs and ys are set to 0.
 **/
Rectangle* create_rectangle(unsigned int size_x, unsigned int size_y);

/**
 * Creates a Rectangle with the dimensions of the box.
 * Start points xs and ys are set to 0.
 **/
Rectangle* create(PixelBox & box);
