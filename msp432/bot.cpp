#include "bot.hpp"

Message::Message(String task_id, String chat_id, String username, String message)
	: task_id(task_id), chat_id(chat_id), username(username), message(message)
{}

Vector<String>* Message::toBoxStrings(CharBox& box, bool display_name, unsigned int name_max_len, bool name_extra_line)
{
	Vector<String> * out = new Vector<String>();
	if(box.height == 0 || box.width == 0)
	{
		return out;
	}
	String line1("");
	if(display_name)
	{
		unsigned int real_name_len = name_max_len;
		if(name_max_len > box.width-1)
		{
			// -1 because of the colon
			real_name_len = box.width-1;
		}
		line1 += username.substr(0, real_name_len);
		line1 += ":";
	}
	bool new_line = name_extra_line | (line1.size() == box.width);
	String tmp(message);
	if(!new_line)
	{
		// line not finished, append rest of message
		line1 += " ";
		unsigned int len = box.width-line1.size();
		if (len>0)
		{
			String sub(tmp.substr(0, len));
			tmp = tmp.substr(len);
			line1 += sub;
		}
	}
	out->push_back(line1);
	for(unsigned int i=1; i<box.height; i++)
	{
		// append message lines
		String msg_part(tmp.substr(0, box.width));
		tmp = tmp.substr(box.width);
		out->push_back(msg_part);
	}
	return out;
}
