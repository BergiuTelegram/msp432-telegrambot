#pragma once

#include <cstring>
#include <cstdio>
#include <queue>
#include <cstdlib>
#include <string.h>

#include <String.h>

#include "vector.hpp"
#include "geometrics.hpp"


/**
 * Message that can be displayed on the display
 **/
struct Message
{
	String task_id;
	String chat_id;
	String username;
	String message;

	Message(String task_id, String chat_id, String username, String message);

	/**
	 * The Box's height and width are the seen as the amount of chars that fit in
	 * an area. This function returns an vector of strings that fits in this box.
	 *
	 * bool display_name:
	 *    true:  the name will be displayed
	 *    false: the name will not be displayed
	 * unsigned int name_max_len:
	 *    the name will be cut to NAME_MAX_LEN chars
	 * bool name_extra_line:
	 *    true:  the name will be in an extra line
	 *    false: the message is put into the same line as the name
	 **/
	Vector<String>* toBoxStrings(CharBox& box, bool display_name=true, unsigned int name_max_len=-1, bool name_extra_line=true);
};
