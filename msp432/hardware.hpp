#pragma once

#include <uart_msp432.h>
#include <std_io.h>
#include <gpio_msp432.h>
#include <spi_msp432.h>
#include <String.h>

#include <st7735s_drv.h>
#include <uGUI.h>
#include <uGUI_colors.h>

#include "menu.hpp"

uart_msp432 uart; // default is backchannel UART!

// Forward any chars from the ESP8266 UART to the back channel UART
// so we can see the debug messages from the ESP8266!
void uart_esp_rx_handler(char c) {
	uart.putc(c);
}

struct EspUartConnection
{
	uart_msp432 * uart_esp;

	EspUartConnection(uart_msp432 * _uart_esp=nullptr)
		: uart_esp(_uart_esp)
	{}

	~EspUartConnection()
	{
		delete uart_esp;
	}

	// Initialize the UART which is connected to the ESP8266
	// and handle all incoming chars via an interrupt routine
	static EspUartConnection * create_default()
	{
		std_io::inst.redirect_stdin ( uart );
		std_io::inst.redirect_stdout( uart );
		auto uart_esp = new uart_msp432(EUSCI_A3,115200);
		uart_esp->uartAttachIrq(uart_esp_rx_handler);
		auto out = new EspUartConnection(uart_esp);
		return out;
	}

};

struct EspSpiConnection
{
	gpio_msp432_pin * esp_spi_cs;
	spi_msp432 * esp_spi;

	EspSpiConnection(gpio_msp432_pin * _esp_spi_cs=nullptr, spi_msp432 * _esp_spi=nullptr)
		: esp_spi_cs(_esp_spi_cs), esp_spi(_esp_spi)
	{}

	~EspSpiConnection()
	{
		delete esp_spi;
		delete esp_spi_cs;
	}

	// Initialize the SPI interface which is connected to the
	// ESP8266 and use the client mode on MSP432 side
	static EspSpiConnection * create_default()
	{
		auto esp_spi_cs = new gpio_msp432_pin(PORT_PIN(10, 0));
		auto esp_spi = new spi_msp432(EUSCI_B3_SPI, *esp_spi_cs, SPI::CLIENT);
		/* esp_spi.setSpeed(250000); */
		auto out = new EspSpiConnection(esp_spi_cs, esp_spi);
		return out;
	}

};

struct EspResetConnection
{
	gpio_msp432_pin * esp_reset;

	EspResetConnection(gpio_msp432_pin * _esp_reset=nullptr)
		: esp_reset(_esp_reset)
	{}

	~EspResetConnection()
	{
		delete esp_reset;
	}

	static EspResetConnection * create_default()
	{
		auto esp_reset = new gpio_msp432_pin( PORT_PIN(10, 5) );
		esp_reset->gpioMode(GPIO::OUTPUT | GPIO::INIT_LOW);
		esp_reset->gpioWrite( HIGH );
		auto out = new EspResetConnection(esp_reset);
		return out;
	}

};

struct EspConnection
{
	EspUartConnection * esp_uart_con;
	EspSpiConnection * esp_spi_con;
	EspResetConnection * esp_reset_con;

	EspConnection(EspUartConnection * _esp_uart_con=nullptr,
			EspSpiConnection * _esp_spi_con=nullptr,
			EspResetConnection * _esp_reset_con=nullptr)
		: esp_uart_con(_esp_uart_con), esp_spi_con(_esp_spi_con),
		esp_reset_con(_esp_reset_con)
	{}

	~EspConnection()
	{
		delete esp_reset_con;
		delete esp_spi_con;
		delete esp_uart_con;
	}

	static EspConnection * create_default()
	{
		auto esp_uart_con = EspUartConnection::create_default();
		auto esp_spi_con = EspSpiConnection::create_default();
		auto esp_reset_con = EspResetConnection::create_default();
		auto out = new EspConnection(esp_uart_con, esp_spi_con, esp_reset_con);
		return out;
	}

};

struct Msp
{
	Display * display;
	MenuMessageList * menu;
	EspConnection * esp_con;

	Msp(Display * _display=nullptr, MenuMessageList * _menu=nullptr,
			EspConnection * _esp_con=nullptr)
		: display(_display), menu(_menu), esp_con(_esp_con)
	{}

	static Msp * create_default()
	{
		// Initialize the display and the message list
		auto display = Display::create_default();
		auto menu = MenuMessageList::create_default(display);
		auto esp_con = EspConnection::create_default();
		auto out = new Msp(display, menu, esp_con);
		return out;
	}

	~Msp()
	{
		delete esp_con;
		delete menu;
		delete display;
	}

};
