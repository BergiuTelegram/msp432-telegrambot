/*******************************************************************
 *  An example of bot that echos back any messages received         *
 *                                                                  *
 *  written by Giacarlo Bacchio (Gianbacchio on Github)             *
 *  adapted by Brian Lough                                          *
 *******************************************************************/
#include <ESP8266WiFi.h>
#include <WiFiClientSecure.h>
#include <UniversalTelegramBot.h>
#include "SPI.h"

// Initialize Wifi connection to the router
char ssid[] = "TopSecret";     // your network SSID (name)
char password[] = "bdlq2626"; // your network key

// Initialize Telegram BOT
#define BOTtoken "603989685:AAEBNMoZdmd6ldXrAb3LqrE0z9xNaOeIxCI"  // your Bot Token (Get from Botfather)

WiFiClientSecure client;
UniversalTelegramBot bot(BOTtoken, client);

int Bot_mtbs = 1000; //mean time between scan messages
long Bot_lasttime;   //last time messages' scan has been done


void setup() {
	Serial.begin(115200);

	// Set WiFi to station mode and disconnect from an AP if it was Previously
	// connected
	WiFi.mode(WIFI_STA);
	WiFi.disconnect();
	delay(100);

	// Initialize the SPI interface.
	// The ESP is the SPI master,
	// and running the SPI interface with 1MHz
	SPI.begin();
	SPI.setDataMode(SPI_MODE0);
	SPI.setBitOrder(MSBFIRST);
	SPI.setHwCs(true);
	SPI.setFrequency(1000000);

	// Attempt to connect to Wifi network:
	Serial.print("Connecting Wifi: ");
	Serial.println(ssid);
	WiFi.begin(ssid, password);

	while (WiFi.status() != WL_CONNECTED) {
		Serial.print(".");
		delay(500);
	}

	Serial.println("");
	Serial.println("WiFi connected");
	Serial.print("IP address: ");
	Serial.println(WiFi.localIP());
}


void loop() {
	if (millis() > Bot_lasttime + Bot_mtbs)  {
		int numNewMessages = bot.getUpdates(bot.last_message_received + 1);

		while(numNewMessages) {
			for (int i=0; i<numNewMessages; i++) {
				// create buffers
				const size_t buflen = 100;
				uint8_t sendbuf[buflen] = {0};
				uint8_t recvbuf[buflen] = {0};
				uint8_t recvbuf_trash[buflen] = {0};
				uint8_t sendbuf_trash[buflen] = {0};
				for(int i =0; i<buflen; i++){
					sendbuf[i] = '\0';
					recvbuf[i] = '\0';
					recvbuf_trash[i] = '\0';
					sendbuf_trash[i] = '\0';
				}
				// create msg to send to the msp
				String chat_id = bot.messages[i].chat_id;
				String msg = bot.messages[i].text;
				String send = chat_id+";"+msg;
				// save send msg in sendbuf
				strncpy(reinterpret_cast<char *>(sendbuf), send.c_str(), buflen);
				strncpy(reinterpret_cast<char *>(sendbuf_trash), "TEST;VONESP", buflen);
				Serial.print("ESP: TG: new message: ");
				Serial.println(send.c_str());
				// send msg to msp
				SPI.transferBytes(sendbuf, recvbuf_trash, buflen);
				recvbuf_trash[buflen-1] = '\0';
				delay(5000);
				String recv_trash((char*)recvbuf_trash);
				Serial.print("ESP: MSP: transfered messages trash: ");
				Serial.println(recv_trash);
				// recv reply from msp
				delay(5000);
				SPI.transferBytes(sendbuf_trash, recvbuf, buflen);
				recvbuf_trash[buflen-1] = '\0';
				// save recved msg as string
				delay(2000);
				String recv((char*)recvbuf);
				Serial.print("ESP: MSP: transfered messages: ");
				Serial.println(recv);
				// split it
				int cut_index = recv.indexOf(';');
				String reply_chat_id = recv.substring(0,cut_index);
				String reply_msg = recv.substring(cut_index);
				Serial.print("ESP: split messages chat id: ");
				Serial.println(reply_chat_id);
				Serial.print("ESP: split messages msg: ");
				Serial.println(reply_msg);
				// send reply to tg
				// get updates
				numNewMessages = bot.getUpdates(bot.last_message_received + 1);
			}
		}

		Bot_lasttime = millis();
	}
}
