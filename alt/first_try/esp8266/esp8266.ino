/*******************************************************************
 *  An example of bot that echos back any messages received         *
 *                                                                  *
 *  written by Giacarlo Bacchio (Gianbacchio on Github)             *
 *  adapted by Brian Lough                                          *
 *******************************************************************/
#include <ESP8266WiFi.h>
#include <WiFiClientSecure.h>
#include <UniversalTelegramBot.h>
#include "SPI.h"
#include <queue>

// Initialize Wifi connection to the router
char ssid[] = "";     // your network SSID (name)
char password[] = ""; // your network key

// Initialize Telegram BOT
#define BOTtoken ""  // your Bot Token (Get from Botfather)

WiFiClientSecure client;
UniversalTelegramBot bot(BOTtoken, client);

int Bot_mtbs = 1000; //mean time between scan messages
long Bot_lasttime;   //last time messages' scan has been done


struct TransferItem
{
	String chat_id;
	String msg;

	TransferItem()
	{}

	TransferItem(String chat_id="0", String msg=""): chat_id(chat_id), msg(msg)
	{}

	~TransferItem()
	{}

	String serialize()
	{
		return chat_id+";"+msg;
	}

};


struct TransferQueue
{
	//FIFO
	std::queue<TransferItem*> sendQueue;
	std::queue<TransferItem*> recvQueue;

	TransferQueue(): sendQueue(), recvQueue()
	{}

	// delete all TransferItems?
	~TransferQueue()
	{}

	/**
	 * send the last message and add the received message to the lists
	 *
	 * returns false if all messages are transfered
	 **/
	bool transfer()
	{
		Serial.println("esp: transfer");
		Serial.println("esp: transfer: sendQueue.size(): "+String(sendQueue.size()));
		Serial.println("esp: transfer: recvQueue.size(): "+String(recvQueue.size()));
		// create buffers
		const size_t buflen = 4096;
		uint8_t *sendbuf = new uint8_t[buflen];
		uint8_t *recvbuf = new uint8_t[buflen];
		// if sendqueue is not empty, save the message in the buffer
		if(!sendQueue.empty())
		{
			Serial.println("esp: transfer: send queue not empty");
			Serial.println("esp: transfer: send "+sendQueue.front()->serialize());
			TransferItem *send = sendQueue.front();
			strncpy(reinterpret_cast<char *>(sendbuf), send->serialize().c_str(), buflen);
		} else {
			Serial.println("esp: transfer: send queue empty");
		}
		// send and receive messages
		for(int i=0; i<10000; i++);
		SPI.transferBytes(sendbuf, recvbuf, buflen);
		for(int i=0; i<10000; i++);
		// cast buffer to string
		String s_recv(reinterpret_cast<char const *>(recvbuf));
		Serial.println("esp: transfer: recv: "+s_recv);
		// save if recv is empty
		bool recv_empty = false;
		if(s_recv == ""){
			recv_empty = true;
			// cut at ;
			int cut_index = s_recv.indexOf(';');
			String chat_id = s_recv.substring(0,cut_index);
			String msg = s_recv.substring(cut_index);
			// create new received message
			recvQueue.emplace(new TransferItem(chat_id, msg));
		}
		if(!sendQueue.empty())
			sendQueue.pop();
		Serial.println("esp: transfer: sendQueue.size(): "+String(sendQueue.size()));
		Serial.println("esp: transfer: recvQueue.size(): "+String(recvQueue.size()));
		Serial.println("esp: transfer: return");
		return recv_empty && sendQueue.empty();
	}

	void add_message(String chat_id, String msg)
	{
		Serial.println("esp: add message: from "+chat_id+": "+msg);
		sendQueue.emplace(new TransferItem(chat_id, msg));
	}

	void send_message()
	{
		Serial.println("esp: send_message");
		Serial.println("esp: send_message: sendQueue.size(): "+String(sendQueue.size()));
		Serial.println("esp: send_message: recvQueue.size(): "+String(recvQueue.size()));
		if(recvQueue.empty()){
			Serial.println("esp: send_message: send no message, because queue is empty");
			return;
		}
		TransferItem *out = recvQueue.front();
		String chat_id = out->chat_id;
		String msg = out->msg;
		Serial.println("esp: send_message: send message to "+String(chat_id)+": "+String(msg));
		bot.sendMessage(chat_id, msg, "");
		Serial.println("esp: send_message: pop");
		recvQueue.pop();
		Serial.println("esp: send_message: delete out");
		delete out;
		Serial.println("esp: send: sendQueue.size(): "+String(sendQueue.size()));
		Serial.println("esp: send: recvQueue.size(): "+String(recvQueue.size()));
		Serial.println("esp: send: return");
	}

	void add_all(int numNewMessages)
	{
		Serial.println("esp: add_all");
		for (int i=0; i<numNewMessages; i++) {
			String chat_id = bot.messages[i].chat_id;
			String msg = bot.messages[i].text;
			add_message(chat_id, msg);
		}
	}

	void transfer_all()
	{
		Serial.println("esp: transfer_all");
		while(transfer());
	}

	void send_all()
	{
		Serial.println("esp: send_all");
		while(!recvQueue.empty())
		{
			send_message();
		}
	}

	void my_loop(int numNewMessages)
	{
		Serial.println("esp: Myloop");
		// not timeintensive
		add_all(numNewMessages);
		// time intensive?
		transfer_all();
		// time intensive and maybe limited from telegram
		send_all();
	}

};


TransferQueue tq;
void setup() {
	Serial.begin(115200);

	// Set WiFi to station mode and disconnect from an AP if it was Previously
	// connected
	WiFi.mode(WIFI_STA);
	WiFi.disconnect();
	delay(100);

	// Initialize the SPI interface.
	// The ESP is the SPI master,
	// and running the SPI interface with 1MHz
	SPI.begin();
	SPI.setDataMode(SPI_MODE0);
	SPI.setBitOrder(MSBFIRST);
	SPI.setHwCs(true);
	SPI.setFrequency(250000);

	// Attempt to connect to Wifi network:
	Serial.print("Connecting Wifi: ");
	Serial.println(ssid);
	WiFi.begin(ssid, password);

	while (WiFi.status() != WL_CONNECTED) {
		Serial.print(".");
		delay(500);
	}

	Serial.println("");
	Serial.println("WiFi connected");
	Serial.print("IP address: ");
	Serial.println(WiFi.localIP());
}


void loop() {
	if (millis() > Bot_lasttime + Bot_mtbs)  {
		int numNewMessages = bot.getUpdates(bot.last_message_received + 1);

		while(numNewMessages) {
			Serial.println("esp: New Messages");
			tq.my_loop(numNewMessages);
			numNewMessages = bot.getUpdates(bot.last_message_received + 1);
		}

		Bot_lasttime = millis();
	}
}
