#!/bin/bash
if [[ -e compile_commands.json ]]; then
	cp compile_commands.json .compile_commands.json~
fi
echo "[" > compile_commands.json

first=1

make clean
make | while read in; do
	if [[ `echo $in | grep "^C++ .*.cpp$"` ]]; then
		filename=`echo $in | sed "s/^C++ //g"`
	fi
	if [[ `echo $in | grep "^C .*.c$"` ]]; then
		filename=`echo $in | sed "s/^C //g"`
	fi
	if [[ `echo $in | grep "\@"` ]]; then
		echo $in;
		cmd=`echo ${in:2} | sed "s/-march=[^ ]* //g"`
		if [[ $first -eq 1 ]]; then
			echo "{" >> compile_commands.json
			first=0
		else
			echo ",{" >> compile_commands.json
		fi
		echo "\"directory\": \"$(pwd)\",\"command\": \"${cmd}\",\"file\": \"$(pwd)/${filename}\""} >> compile_commands.json
	fi
done

echo "]" >> compile_commands.json
