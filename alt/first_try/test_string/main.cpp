// ---------------------------------------------
//           This file is part of
//      _  _   __    _   _    __    __
//     ( \/ ) /__\  ( )_( )  /__\  (  )
//      \  / /(__)\  ) _ (  /(__)\  )(__
//      (__)(__)(__)(_) (_)(__)(__)(____)
//
//     Yet Another HW Abstraction Library
//      Copyright (C) Andreas Terstegge
//      BSD Licensed (see file LICENSE)
//
// ---------------------------------------------
//
// This is a demo for SPI communication between
// the WifiTick board (ESP8266) and the MSP432 launchpad.
// The program does 2 things:
//
// - Forward any char which is received from the
//   ESP8266 uart to our backchannel uart, so we
//   can see messages from the ESP8266 on our terminal.
//
// - Send a message via a SPI interface to the ESP8266.
//   The mesage will be returned by the ESP8266 program.
//
//////////////////////// IMPORTANT //////////////////////////
// General hint:
// Make sure this program is running at 48MHz, because with
// lower frequencies, the SPI communication might loose data.
// The ESP8266 is the SPI master, and has a SPI clock rate
// of 1MHz. If the MSP is e.g. only running at 3MHz, it is
// too slow the get all the data via SPI!
/////////////////////////////////////////////////////////////

#include <cstring>
#include <cstdio>
#include <queue>
#include <cstdlib>
#include <string.h>

#include "uart_msp432.h"
#include "std_io.h"
#include "gpio_msp432.h"
#include "spi_msp432.h"
#include "String.h"

uart_msp432 uart; // default is backchannel UART!

void uart_esp_rx_handler(char c) {
	// Forward any chars from the ESP8266 UART to the back channel UART
	// so we can see the debug messages from the ESP8266!
	uart.putc(c);
}

void sleep(int sec)
{
	long ticks = sec*5000000;
	for(long i = 0; i < ticks; i++)
	{}
}

int main(void)
{
	std_io::inst.redirect_stdin ( uart );
	std_io::inst.redirect_stdout( uart );

	// Initialize the UART which is connected to the ESP8266
	// and handle all incoming chars via an interrupt routine
	uart_msp432 uart_esp(EUSCI_A3,115200);
	uart_esp.uartAttachIrq(uart_esp_rx_handler);

	int x = 0;
	while (true) {
		String s("1234567;GeloMyrtol");
		printf("%5i - %s\n", x, s.c_str());
		size_t pos = s.find(';');
		String id = s.substr(0, pos);
		String msg = s.substr(pos+1);
		printf("      - id:  %s\n", id.c_str());
		printf("      - msg: %s\n", msg.c_str());
		sleep(1);
		x++;
	}
	return 0;
}
