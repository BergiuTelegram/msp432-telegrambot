// ---------------------------------------------
//           This file is part of
//      _  _   __    _   _    __    __
//     ( \/ ) /__\  ( )_( )  /__\  (  )
//      \  / /(__)\  ) _ (  /(__)\  )(__
//      (__)(__)(__)(_) (_)(__)(__)(____)
//
//     Yet Another HW Abstraction Library
//      Copyright (C) Andreas Terstegge
//      BSD Licensed (see file LICENSE)
//
// ---------------------------------------------
//
// A simple example for displaying text, lines and
// circles on the LCD using the uGUI library.
// See the doc folder in YAHAL/src/uGUI for more
// information on the uGUI API.

#include "gpio_msp432.h"
#include "spi_msp432.h"
#include "st7735s_drv.h"
#include "uGUI.h"
#include "uGUI_colors.h"
#include "pixel_stream_const.h"
#include "font_4x6.h"
#include "font_5x8.h"
#include "font_5x12.h"
#include "font_6x8.h"
#include "font_6x10.h"
#include "font_7x12.h"
#include "font_8x12.h"
#include "gpio_msp432.h"
#include "adc14_msp432.h"

#include <cstdlib>
/* #include <cstring> */
#include <stdio.h>
#include <stdlib.h>
#include <String.h>


class Menu
{
    st7735s_drv* lcd;
    uGUI* gui;
    int box_x;
    int box_y;
    int status_line_h = 9;

public:
    Menu(st7735s_drv* _lcd, uGUI* _gui): lcd(_lcd), gui(_gui), box_x(_lcd->getSizeX()), box_y(_lcd->getSizeY())
    {}

    ~Menu()
    {}

    void print_statusline_sites(String mode, int position, int len)
    {
        gui->SetForecolor(C_GREEN);
        gui->FontSelect(&FONT_6X8);
        int font_x = 8;
        int font_y = 6;
        int pl = 2; // padding left
        int pr = 2; // padding right
        int pt = ((status_line_h-1/*border top 1px*/) - font_y) / 2; // padding top
        int xs = 0;
        int xe = box_x-1;
        int ys = box_y-1-status_line_h;
        gui->DrawLine(xs, ys, xe, ys, C_GREEN);
        gui->PutString(pl, ys+pt, mode);
        String site = to_String(position) + "/" + to_String(len);
        // TODO: wieso 7?
        int site_len = site.size()*(font_x-1);
        gui->PutString((box_x-1)-site_len-pr, (ys+1)+pt, site);
    }

    void print_chatliste(String* chats, int chats_len, int selected_chat)
    {
        int height = 8;
        int pl = 4; // padding left
        int pr = 6; // padding right
        int pt = 4; // padding top
        int items_per_page = 10;
        int page = selected_chat / items_per_page;
        int offset = page*items_per_page;
        gui->FontSelect(&FONT_5X8);
        for(int i=0; i<chats_len-offset; i++)
        {
            String chat = chats[i+offset];
            int text_x = pt+(height+3)*i;
            int line_x = pt+(height+3)*(i+1)-2;
            if (line_x<box_y-status_line_h) {
                if (i+offset==selected_chat)
                {
                    gui->SetForecolor(C_LIGHT_GREEN);
                    String text = ">";
                    text += chat;
                    gui->PutString(pl, text_x, text);
                } else {
                    gui->SetForecolor(C_GREEN);
                    gui->PutString(pl, text_x, chat);
                }
                if (i+offset==selected_chat || i+offset==selected_chat-1)
                {
                    gui->DrawLine(pl, line_x, box_x-pr, line_x, 0x555555);
                } else
                {
                    gui->DrawLine(pl, line_x, box_x-pr, line_x, 0x111111);
                }
            }
        }
    }

    void menu_chat_liste(String* chats, int chats_len)
    {
        int selected_chat = 9;
        print_chatliste(chats, chats_len, selected_chat);
        print_statusline_sites("Chat Liste", selected_chat+1, chats_len);
    }

};

int nomain(void)
{
    // Setup SPI interface
    gpio_msp432_pin lcd_cs (PORT_PIN(5, 0));
    spi_msp432  spi(EUSCI_B0_SPI, lcd_cs);
    spi.setSpeed(24000000);
    spi.useCS(false);

    // Setup LCD driver
    gpio_msp432_pin lcd_rst(PORT_PIN(5, 7));
    gpio_msp432_pin lcd_dc (PORT_PIN(3, 7));
    st7735s_drv* lcd = new st7735s_drv(spi, lcd_rst, lcd_dc, st7735s_drv::Crystalfontz_128x128);

    // Setup uGUI
    uGUI* gui = new uGUI(*lcd);

    lcd->clearScreen(0x0);

    int chats_len = 20;
    String * chats = (String*) calloc(sizeof(String), chats_len);
    for(int i=0; i<chats_len; i++)
    {
        chats[i] = "";
    }
    chats[0] = "Notizen";
    chats[1] = "Jasmin";
    chats[2] = "jojo";
    chats[3] = "TheVillage #rathaus";
    chats[4] = "TheVillage #taverne";
    chats[5] = "RWTH Kuchen";
    chats[6] = "RWTH DRM";
    chats[7] = "RWTH LSD";
    chats[8] = "RWTH Drogen";
    chats[9] = "RWTH Vim";
    chats[10] = "RWTH Emacs";
    chats[11] = "RWTH RANTING";
    chats[12] = "TheVillage #a";
    chats[13] = "TheVillage #b";
    chats[14] = "TheVillage #c";
    chats[15] = "TheVillage #d";
    chats[16] = "TheVillage #e";


    Menu menu(lcd, gui);
    menu.menu_chat_liste(chats, chats_len);

    free(chats);
    delete gui;
    delete lcd;
    return 0;
}


// create ADC channels
adc14_msp432_channel joy_X(15);
adc14_msp432_channel joy_Y( 9);
uint16_t offset_X, offset_Y;

// GPIO Pins with LEDs
gpio_msp432_pin red_led  (PORT_PIN(2, 0));
gpio_msp432_pin green_led(PORT_PIN(2, 1));
gpio_msp432_pin blue_led (PORT_PIN(2, 2));

void delay() {
    for(int i=0; i < 5000; ++i) ;
}

void handler_X(uint16_t, uint16_t value) {
    // This handler is called after EVERY conversion
    // of a new ADC value.
    //
    // Switch on/off the red LED if the joystick
    // is moved left/right
    if ( (value-offset_X) >  200 ) red_led.gpioWrite(HIGH);
    if ( (value-offset_X) < -200 ) red_led.gpioWrite(LOW);
}

void handler_Y(uint16_t, uint16_t irq_mode) {
    // This handler is only called, when the
    // configured window mode is met.
    // Before this handler is called, the window
    // interrupts are disabled. So you need to
    // make another call to attachWinIrq(...),
    // if the handler should be called again.
    // Of course the window values can be changed
    // if desired.
    //
    // Flash the red/green/blue LED when the joystick
    // is within a defined window.
    if ( irq_mode == ADC::ABOVE_HIGH ) {
        green_led.gpioWrite(HIGH);
        // Set a new window so the handler is called
        // again, when the Y joystick is near the middle
        joy_Y.attachWinIrq (handler_Y,
                            offset_Y-40, offset_Y+40,
                            ADC::WITHIN_WIN);
        delay();
        green_led.gpioWrite(LOW);
    }
    if ( irq_mode == ADC::BELOW_LOW  ) {
        red_led.gpioWrite(HIGH);
        // Set a new window so the handler is called
        // again, when the Y joystick is near the middle
        joy_Y.attachWinIrq (handler_Y,
                            offset_Y-40, offset_Y+40,
                            ADC::WITHIN_WIN);
        delay();
        red_led.gpioWrite(LOW);
    }
    if ( irq_mode == ADC::WITHIN_WIN) {
        // okay, we are in the middle again. Set a new
        // window for LED on/off function.
        // Also flash the blue LED!
        blue_led.gpioWrite(HIGH);
        joy_Y.attachWinIrq (handler_Y,
                            offset_Y-400, offset_Y+400,
                            ADC::BELOW_LOW | ADC::ABOVE_HIGH);
        delay();
        blue_led.gpioWrite(LOW);
    }
}

int main2()
{
    // set the modes
    red_led.  gpioMode(GPIO::OUTPUT);
    green_led.gpioMode(GPIO::OUTPUT);
    blue_led.gpioMode (GPIO::OUTPUT);
    joy_X.adcMode(ADC::ADC_10_BIT);
    joy_Y.adcMode(ADC::ADC_10_BIT);

    // read the initial value (middle position)
    offset_X = joy_X.adcReadRaw();
    offset_Y = joy_Y.adcReadRaw();

    // attach irq handlers
    joy_X.attachScanIrq(handler_X);
    joy_Y.attachWinIrq (handler_Y,
                        offset_Y-400, offset_Y+400,
                        ADC::ABOVE_HIGH | ADC::BELOW_LOW);

    // shortcut for adc instance
    adc14_msp432 & adc = adc14_msp432::inst;

    // setup and start the scan process
    adc.adcSetupScan(ADC::ADC_10_BIT);
    adc.adcStartScan(9, 15);

    while(true) {
        // nothing to do in the main loop,
        // because everything is handled by
        // the interrupt handlers!
    }
}
