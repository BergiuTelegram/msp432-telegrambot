////////////////////////////////////////////////////////////
// This is the test program which is running on the ESP8266.
// Use the WifiTick-Programmer to download this program
// to the WifiTick board!
////////////////////////////////////////////////////////

#include <ESP8266WiFi.h>
#include <WiFiClientSecure.h>
#include <UniversalTelegramBot.h>
/* #include <ArduinoSTL.h> */
#include <list>
#include "Arduino.h"
#include "SPI.h"
#include "Tasks.h"

// copy secret.h.template to secret.h and add your wifi credentials and bot token
#include "secret.h"

WiFiClientSecure client;
UniversalTelegramBot bot(BOTtoken, client);

int Bot_mtbs = 1000; //mean time between scan messages
long Bot_lasttime;   //last time messages' scan has been done

ESP_Task_Container * esp_task_cont;

void setup() {
    // Initialize the serial port
    Serial.begin(115200);

    // Initialize the SPI interface.
    // The ESP is the SPI master,
    // and running the SPI interface with 1MHz
    SPI.begin();
    SPI.setDataMode(SPI_MODE0);
    SPI.setBitOrder(MSBFIRST);
    SPI.setHwCs(true);
    SPI.setFrequency(250000);

	// Set WiFi to station mode and disconnect from an AP if it was Previously
	// connected
	WiFi.mode(WIFI_STA);
	WiFi.disconnect();
	delay(100);

	// Attempt to connect to Wifi network:
	Serial.print("Connecting Wifi: ");
	Serial.println(ssid);
	WiFi.begin(ssid, password);

	while (WiFi.status() != WL_CONNECTED) {
		Serial.print(".");
		delay(500);
	}

	Serial.println("");
	Serial.println("WiFi connected");
	Serial.print("IP address: ");
	Serial.println(WiFi.localIP());
	Serial.println("Waiting 5 seconds:");
	for (int i = 0; i < 5; i++) {
		Serial.print(".");
		delay(1000);
	}
	Serial.print("\n");

	// creating task container
	esp_task_cont = new ESP_Task_Container();
}

void loop() {

	if (millis() > Bot_lasttime + Bot_mtbs)  {
		int numNewMessages = bot.getUpdates(bot.last_message_received + 1);

		while(numNewMessages) {
			for (int i=0; i<numNewMessages; i++) {
				// Send/Receive buffers
				uint8_t txbuf[100];
				uint8_t rxbuf[100];

				// Fill in some data to send
				/* strcpy((char*)txbuf, "This is data from ESP8266!!"); */
				// create msg to send to the msp
				String cmd = "NEW;";
				String task_id = String(bot.messages[i].update_id);
				String chat_id = bot.messages[i].chat_id;
				String from = bot.messages[i].from_name;
				String msg = bot.messages[i].text;
				/* String send = cmd+chat_id+";"+msg+"\0"; */
				MSP_Task* send = new MSP_NEW_Task(task_id, chat_id, from, msg);
				// save send msg in sendbuf
				strncpy(reinterpret_cast<char *>(txbuf), send->toString().c_str(), 100);

				// Perform duplex SPI transfer
				SPI.transferBytes( txbuf, rxbuf, 100 );

				Serial.print  ("ESP8266 received: ");
				Serial.println((const char *)rxbuf);
				String recv((const char *)rxbuf);
				esp_task_cont->add(recv);
				Serial.print("Laenge send: ");
				Serial.println(esp_task_cont->send_tasks.size());
				Serial.print("Laenge ack: ");
				Serial.println(esp_task_cont->ack_tasks.size());
				esp_task_cont->print();


				/* delay(1500); */
				bool get_next_updates = true;
				if(get_next_updates) {
					numNewMessages = bot.getUpdates(bot.last_message_received + 1);
				} else {
					delay(500);
				}
			}
		}
		Bot_lasttime = millis();
	}
}

