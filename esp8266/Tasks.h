/* Commands send to the MSP */
enum MSP_CMD
{
	GET, // no new messages, anything to do?
	NEW, // add a new tg message as task
	EAK  // acknowledge that esp has processed a SND cmd
};

/* Commands received by the ESP */
enum ESP_CMD
{
	NUL, // nothing to do here. should be ignored
	SND, // send this message to the given id. is send after a tg command is executed
	MAK  // acknowledge that msp has processed a NEW cmd
};

//////////////////////
// TASK SEND TO MSP //
//////////////////////
struct MSP_Task
{
	MSP_CMD cmd;
	MSP_Task(MSP_CMD _cmd): cmd(_cmd)
	{}
	virtual String toString() = 0;
};

/* no new messages, anything to do? */
struct MSP_GET_Task: MSP_Task
{
	MSP_GET_Task(): MSP_Task(GET) {}
	String toString()
	{
		String out("GET;");
		return out;
	}
};

/* add a new tg message as task */
struct MSP_NEW_Task: MSP_Task
{
	String task_id;
	String chat_id;
	String username;
	String message;
	MSP_NEW_Task(String task_id, String chat_id, String username, String message)
		: MSP_Task(NEW), task_id(task_id), chat_id(chat_id), username(username), message(message)
	{}
	String toString()
	{
		String out("NEW;"+task_id+";"+chat_id+";"+username+";"+message);
		return out;
	}
};

/* acknowledge that esp has processed a SND cmd */
struct MSP_EAK_Task: MSP_Task
{
	String task_id;
	MSP_EAK_Task(String task_id): MSP_Task(EAK), task_id(task_id)
	{}
	String toString()
	{
		String out("EAK;"+task_id+";");
		return out;
	}
};

//////////////////////////
// TASK RECEIVED ON ESP //
//////////////////////////
struct ESP_Task
{
	ESP_CMD cmd;
	ESP_Task(ESP_CMD _cmd): cmd(_cmd)
	{}
	virtual String toString() = 0;
};

/* an empty task that does nothing an should not even be created */
struct ESP_NUL_Task: ESP_Task
{
	ESP_NUL_Task(): ESP_Task(NUL) {}
	String toString()
	{
		String out("NUL;");
		return out;
	}
};

/* send a message to telegram */
struct ESP_SND_Task: ESP_Task
{
	String task_id;
	String chat_id;
	String message;
	ESP_SND_Task(String task_id, String chat_id, String message)
		: ESP_Task(SND), task_id(task_id), chat_id(chat_id), message(message)
	{}
	static ESP_SND_Task* create(String body)
	{
		int cut_index = body.indexOf(';');
		if (cut_index <= 0) {
			return nullptr;
		}
		String task_id = body.substring(0,cut_index);
		String tmp = body.substring(cut_index+1);
		cut_index = tmp.indexOf(';');
		if (cut_index <= 0) {
			return nullptr;
		}
		String chat_id = tmp.substring(0,cut_index);
		// XXX: Empty messages?
		String message = tmp.substring(cut_index+1);
		ESP_SND_Task* out = new ESP_SND_Task(task_id, chat_id, message);
		return out;
	}
	String toString()
	{
		String out("SND;"+task_id+";"+chat_id+";"+message);
		return out;
	}

};

/* acknowledge that msp has processed a NEW cmd */
struct ESP_MAK_Task: ESP_Task
{
	String task_id;
	ESP_MAK_Task(String task_id): ESP_Task(MAK), task_id(task_id)
	{}
	static ESP_MAK_Task* create(String body)
	{
		int cut_index = body.indexOf(';');
		if (cut_index <= 0) {
			return nullptr;
		}
		String task_id = body.substring(0,cut_index);
		ESP_MAK_Task* out = new ESP_MAK_Task(task_id);
		return out;
	}
	String toString()
	{
		String out("MAK;"+task_id+";");
		return out;
	}
};

///////////////
// FACTORIES //
///////////////
struct ESP_Task_Factory
{
	static ESP_Task* create(String recv)
	{
		int cut_index = recv.indexOf(';');
		if(cut_index <= 0) {
			return nullptr;
		}
		String s_cmd = recv.substring(0,cut_index);
		String body = recv.substring(cut_index+1);
		if(s_cmd=="NUL")
		{
			// do nothing?
			return nullptr;
		} else if(s_cmd=="SND")
		{
			ESP_SND_Task* in = ESP_SND_Task::create(body);
			return in;
		} else if(s_cmd=="MAK")
		{
			ESP_MAK_Task* in = ESP_MAK_Task::create(body);
			return in;
		}
	}
};

///////////////
// CONTAINER //
///////////////
struct ESP_Task_Container
{
	std::list<ESP_SND_Task*> send_tasks;
	std::list<ESP_MAK_Task*> ack_tasks;
	ESP_Task_Container(): send_tasks(), ack_tasks()
	{}
	void add(String recv)
	{
		ESP_Task* taskptr = ESP_Task_Factory::create(recv);
		if(taskptr==nullptr)
		{
			return;
		} else if(taskptr->cmd==NUL)
		{
			// should not happen
			return;
		} else if(taskptr->cmd==SND)
		{
			send_tasks.push_front((ESP_SND_Task*)taskptr);
		} else if(taskptr->cmd==MAK)
		{
			ack_tasks.push_front((ESP_MAK_Task*)taskptr);
		}
	}
	void print()
	{
		std::list<ESP_SND_Task*> print_send(send_tasks);
		std::list<ESP_MAK_Task*> print_ack(ack_tasks);
		if(print_send.size()>0) {
			for(ESP_Task* t: print_send)
			{
				Serial.println("  "+t->toString());
			}
		}
		if(print_ack.size()>0) {
			for(ESP_Task* t: print_ack)
			{
				Serial.println("  "+t->toString());
			}
		}
	}
};

/* TaskPtr<MSP_Task> recv_tasks[20]; */
