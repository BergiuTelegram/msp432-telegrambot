# MSP432 Telegrambot
This is a simple telegram bot that runs on the MSP432 board from texas instruments. It is connected with the Educational Boosterpack, MK II and can show received messages on the Boosterpacks display.

## Hardware
- MSP432
	- The main board where the bot is running
- Educational Boosterpack MK II
	- An extension for the board, that contains a display, some buttons and some other stuff
- ESP8862
	- for connecting us with wifi and to the telegram servers

## Dependencies esp8266
To install this dependencies open your arduino IDE and goto: Sketch -> Include Library -> Add .ZIP Library...

- [Universal-Arduino-Telegram-Bot](https://github.com/witnessmenow/Universal-Arduino-Telegram-Bot)
	- `Universal-Arduino-Telegram-Bot-1.1.0.zip`
	- telegram bot library for the esp8266
- [ArduinoJson](https://github.com/bblanchon/ArduinoJson)
	- `ArduinoJson-v5.13.2.zip`
		- newer versions don't work
	- dependency for the bot library

## Projekt Beschreibung
The idea is to connect the board with an telegram bot to receive messages on the msp board, execute functions on specific commands and send replys. Some examples are:
- Reading sensors:
	- User writes "/temp" and the board answers with the current temperature
- Controlling Hardware:
	- User writes "/led" and the board switches some leds on or of
	- User writes "/sound" and the board plays some sound with the beeper
	- User writes "/green" and the board makes the connected fairy lights to green

## How
The esp is running a telegram bot library, has access to the internet and can receive and send telegram messages. The boards (msp and esp) are connected and they can transfer messages over SPI. Now when the esp receives a telegram message, it sends the message with some meta information to the msp. The msp saves the message as task, displays it on the Educational Boosterpacks display and replys with an acknowledgment to the esp. when the esp reseives the ack, it tells telegram, that we received the message. During that, the msp parses the task and executes the defined commands. When they are finished the msp can transfer a message to the esp to tell him to write something in the telegram chat. After the esp has written it, it sends an ack to the msp and the msp marks the task as done.

When a board doesn't get an ack, it will repeat the step and send the message again to the board.

## Roadmap
### Communication:
- [ ] send from msp
- [ ] receive from msp
- [ ] send from esp
- [ ] receive from esp

### Menue:
- [ ] no main loop
- [ ] read buttons as A and B
  - [ ] A goes to the next page
  - [ ] B goes to the previous page

### Bot:
- [ ] messages execute some functions
- [ ] command handler
- [ ] implement events
  - [ ] switch LED on/off /light
  - [ ] read sensor data /temp
  - [ ] use buzzer to make noise /sound
  - [ ] display a picture on the display /pic
  - [ ] controll fairy lights /lichterkette

## Useful links
- https://github.com/Terstegge/WiFiTick
	- documentation of the wifi chip (esp8266)

## Transmission over SPI:
### ESP -> MSP (send codes):
- GET
	- get tasks
	- no other messages to transfer, but checking for new tasks
	- `GET;`
- NEW
	- new telegram message
	- needs an acknowledgement
	- only if it gets acknowledged, telegram.getUpdates should be called
	- `NEW;task_id;chat_id;username;msg\0`
- EAK (ESP ACK)
	- done a task successfull (send tg message)
	- `EAK;task_id;`

### MSP -> ESP (receive codes):
- NUL
	- empty response, nothing to do
	- `NUL;`
- SND
	- send message to tg
	- needs an acknowledgement
	- `SND;task_id;chat_id;msg\0`
- MAK (MSP ACK)
	- received the telegram message successfull
	- `MAK;task_id;`

### Special attributes
- task\_id := telegram.message[i].\_id
	- to let the task ids be unique between restarts
